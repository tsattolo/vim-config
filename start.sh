#!/bin/bash
#Run when this repo is first cloned
#Working directory should be the root of the git repo

ln -s $(pwd)/.vimrc ~/.vimrc
ln -s $(pwd)/.gvimrc ~/.gvimrc
ln -s $(pwd) ~/.vim
mkdir ./swap

./non_vim_tweaks.sh
