"Enable pathogen
execute pathogen#infect()

"Misc
syntax on
filetype plugin indent on
set splitright
set swapfile
set dir=~/.vim/swap
set number
set showcmd
set hlsearch

let g:easytags_async=1
let g:easytags_auto_highlight=0

"Tabbing
set tabstop=8
set softtabstop=0
set expandtab
set shiftwidth=4
set smarttab


"Personal
"Set leader
nmap <Space> _
let mapleader = "_"

"Paste yanked text only
nnoremap <leader>p "0p
nnoremap <leader>P "0P
"Insert single character
nnoremap <leader><CR> a <Esc>r
nnoremap <leader><S-CR> i <Esc>r
"Changing windows
nnoremap <leader>h <C-w>h
nnoremap <leader>j <C-w>j
nnoremap <leader>k <C-w>k
nnoremap <leader>l <C-w>l
"Comand mode abbreviation
nnoremap ; :
vnoremap ; :
"Easy recording execution
nnoremap <leader>q @q
nnoremap <leader>w @w
nnoremap <leader>e @e
nnoremap <leader>s @a
nnoremap <leader>a @s


"For seaccow project
set makeprg=./check_syn.sh


"Syntastic settings
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
