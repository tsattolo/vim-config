#!/bin/bash
# First arg must be a git subtree command (most likely add or pull)

cat list | while read line
do
    vars=($line)
    git subtree $1 --prefix bundle/"${vars[0]}"  "${vars[1]}" master --squash
done
